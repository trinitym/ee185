# The Wing Hinge

This folder contains SOLIDWORKS parts and diagrams of the
wing hinge of a Fractal Flyer for the FLIGHT project at Stanford. 
The design of this hinge has gone through many iterations and revisions. The
current design is driven by four major concerns and heavily based on [proposed
improvements to an earlier design](../../design-docs/claire-hinge-proposal.pdf):
  1. Manfacturability. 
     a. Parts (such as bearings) have been chosen to have
     more forgiving (.003"-.005") tolerances, such that the precision of the
     leaves can be lower and there are more options for making them.
	 b. Edges and corners have been chosen to allow pieces to be
     made with fewer rotations and vise configurations as well as have
     less concentrated stres; for example, rather
     than corners between the round and flat parts of the leaves, they
	 have fillets for a gradual thickess change.
  2. Appearance.
	 a. The leaves have an identical cross section around the shaft, for
     visual symmetry.
	 b. The tips of the leaves are pointed, with the same angle as the 
	 wide end of the body diamond.
  3. Structural. 
     a. The wing leaves are one-sided, and are attached to the wing with
	 rivets, using washers to distribute stress. This allows a tight and
	 flush mating between the wing and leaf so that stress from the 
	 wing will not concentrate on a corner or sharp edge.
	 b. The thickness of the leaf and
	 washer sizes have been chosen such that 8mm acrylic should be able to
	 easily sustain the compression of the rivet.
  4. Cost.
     a. The shafts have been reduced to 12 inches and the hinge leaves have
	 been adjusted accordingly, reducing the number of hinge leaves on each
	 side to 3.
	 b. The bearings are simple nylon journal bearings, which for the 
	 rotation speeds and radial loads is sufficient.
     c. Greater options in manufacturability (see above), hopefully one-pass,
	 reduce costs.

## Parts

There are three mechanical parts.
  1. The long body leaf. This leaf is on the wider region of the body. Its
  through holes are designed to match with those of the bearing for the 
  motor shaft inside the body. 
  2. The short body leaf. This leaf is on the narrower part of the body.
  It is shorter so that it can fit and because it does not meed to mate with
  existing holes.
  3. The wing leaf. This leaf is on the wing and coupled to the hinge shaft.
  It has a small carve-out for the aluminum channel on the edge of the wing
  and is attached to the wing with a rivet.
  
There are two long body leaves, each mated with one of the motor shaft bearings. 
These two bearings surround the points on the shafts where the cable between them
is attached, to absorb the radial stress and reduce deflection of the shafts.

**TODO**: decide if we will stick with the current ball bearing design for the motor
shaft or move to Igus pillow block bearings. If we change we will need to revisit
the diameter and spacing of the through-holes on the long leaves.

## Other parts

The shaft is a [12" long, 1/4" diameter chrome-plated 1045 steel shaft](https://www.mcmaster.com/5947K11).

The shaft rotates within the body leaves using a [1/2" long, 3/8" diameter nylon dry bearing.](https://www.mcmaster.com/6389K113).

The shaft is coupled to the wing hinge with a [slotted spring pin.](https://www.mcmaster.com/spring-pins/pins/slotted-spring-pins-6/), diameter/etc. to be decided.

The wing leaves are attached to the wings with 1/8" soft set rivets, offset with 3/8" washers.

The motor shaft and the wing shaft are coupled with a cable, that is brazed to the shafts with a
through-hole. The exact wire is TBD. One possibilty is [3/32" galvanized extra-flexible wire.](https://www.mcmaster.com/3332T51).

## Design Notes

Functionality Requirements:
  * Each wing can move independently
  * Mechanism contained within body
  * Mechanism is attractive and clean (Charlie’s judgement)
Technical Requirements:
  * Does not consume s/ignificant (>0.1W) power when wings are stationary
  * Wing range of motion: [-90, 90] degrees (vertical to vertical with respect to top plate)
  * Wing speed 1-2 RPM
  * Required torque is 4 lb in, 4.3 kg cm
  * Vin = 12V or Vin = 5V
Environmental requirements
  * Last for 5 years of continuous actuations
  * Function in 0-40C
  * Cannot lose/drop parts


Assume we’ll keep the motor the same, such that we do not need a significant redesign of the 
principles of the mechanism: we will use a cable that connects the motor shaft to the wing shaft.


### Bearings and Shaft

The first thing to consider is the bearings and shaft diameter.

I like Claire’s idea of the COTS component body leaf of the wing hinge. If we wanted to do this cheapest and most off-the-shelf, I’d go with that approach. But the hinge has an aesthetic component to it, so I’d like to stick with some sort of custom part that is attractive.

One challenge we ran into with using press-fit ball bearings is the tolerances for press-fitting them: .0005”. For our loads and speeds, however, they are overkill. Instead, I’d like to go with a light duty nylon sleeve bearing. They have a tolerance of .005”, so well within our abilities to make in many different ways and so easy to press-fit.

Given we require 4 lb in of torque, on a ¼ inch shaft this is 32lbs of radial load:

However, if the load is between two bearings, it will be shared across them, in a ratio that’s approximately the inverse of the ratio of the distances. So if the load is evenly between the two bearings it’s shared evenly between them. As a result, we can design the system so each bearing will be subjected to 16lb of radial load.

A ½” nylon dry-running bearing for a ¼” shaft (⅜” OD) has a dynamic radial load capacity of 47lbs at 120PM and costs $0.80. This is not a big cost savings compared to the existing ball bearings, but the more forgiving tolerances give us a lot more flexibility in how we make the hinge leaves.

Sticking with the ¼” shaft will let us use bearings that are well within our requirements. We could go to ⅛” and use MDS-Filled Nylon Bearings at only a small increase in cost. However, it would make it difficult to drill a hole for the cable to be brazed. 

So, let’s go with a ¼” shaft and ½” nylon dry running bearings.

For the shaft material, we can either go with 303 Stainless, which would be harder to machine, or a coated 1045 carbon steel. Chrome-plated is preferable to black oxide as it would keep the shiny look.

A 12” 303 stainless ¼” diameter shaft is 9.08; a chrome-plated 1045 steel is 8.48. Chrome-plated will be easier to work with and is cheaper, so I’d suggest using it.

### Hinge Leaves

The body hinge leaves will need to hold the nylon bearings with an outer diameter of 0.375” with a tolerance of .005”. The wing hinge leaves will need to directly mate with the wing shaft (0.25”) and affix to it somehow so this leaf is coupled to the shaft.

For symmetry, I propose that the two leaves have the same basic geometry. They will differ in three ways.

  1. They may need to differ in length.
  2. The body leaves will have a larger bore: ⅜” vs. ¼”.
  3. The wing leaves need a cutout for the aluminum LED channel: their end will be thicker than the part of the leaf near the shaft.

One advantage of using the same basic geometry is it means that the top of the wing, when parallel to the top plate, will be along the same line as the top plate. We could make it so that this is either true for the acrylic of the wing or for the aluminum channel.

For the basic geometry, I propose sticking with the basic shape currently used for the body leaves: a strip of aluminum that widens to a circle with a bore through it. The circle is ½”, to allow 1/16” around a ⅜” bore for the nylon bearing.

To couple the shaft to the wing leaves, I propose going with Steve’s idea of using a slotted spring pin. This will involve drilling a 1/16” hole in the shaft and the wing leaf. The pin will be ½” long. A 1/16” pin has a breaking strength of 430 lbs so is trivially strong enough.

To ensure that the shaft does not slide through the body leaves, we can use two set screw shaft collars. Alternatively, we can place the wing leaves just within the body ones; with their set pin they will not move and will not allow the shaft to move between the body leaves.

### Stress on Wings

I’ll present some stress analysis simulation results on the wing and hinge assembly, which I ran to try to get a better understanding of the mechanical, material and structural issues. I’ve attached the simulation reports. It’s my second time running a SolidWorks simulation and I don’t know how to navigate its report system well. The report isn’t super clean, I apologize. The key properties of the simulation were:

Hinges modeled as 6061-T6 aluminum and are fixed (so the wing is effectively a clamped plate with free edges).
Rivets modeled as 1/8" 5052-H24 aluminum bolts with a preload of 250lbs and head/nut diameter of 0.250in (this changes below for the final simulation).
Wing modeled as 8mm PMMA.
To examine worst case and handle a X8 weight, I placed a 150N load distributed evenly across the wing.

Please note that the deformation visualizations exaggerate for visibility.

Results from a two-sided (full) hinge, the current design: 
 - The peak stress is on the hinges, 76MPa. This is from the preloading (e.g., the rivet compression force). The compressive strength of 6061-T6 is > 60GPa.
 - The maximum stress on the PMMA is under the hinge, 9.6MPa.  The compressive strength of PMMA is 103MPa. 
 - The mate edge between the hinge and wing does not have significantly higher stress, because it's not a joint.
 - The stress of the preloading and the wing itself do not add to one another: https://mechanicalc.com/reference/bolted-joint-analysis
 - If we did have a joint, the filet should be out, not in: please look at slide 15 of chapter 9 of the FUNdaMENTALs of Design from MIT, used in the intro design course (2.007): http://pergatory.mit.edu/2.007/resources/FUNdaMENTALS.html

If we go to a half-hinge design (also attached) all the results are equivalent, except:
 - The peak stress becomes the PMMA at the end the rivet directly on the PMMA, with a stress of 37MPa. This is what you’d expect — all of the stress of the preloading is on that little rivet head/tail. This is 1/3 of the compressive strength of PMMA so probably not good.

If we go to a half-hinge design and use a wide (0.375”) washer on the PMMA, all the results are similar, except:
 - The peak stress on the lower PMMA drops to 8.5 MPa. The rivet washer distributes the stress mostly along its edge.
 - The peak stress on the PMMA, 21MPa, is now underneath the hinge (because of the cones of stress directly down from the rivet head).

If you reduce the preload to 10N (so ~2 pounds), the maximum stress on the wing is 320KPa, around the hinge. This is ~1/300th of 103MPa.

In summary, my conclusion even with 15 pounds on the wing, the hinge holding the wing introduces stresses >2 orders of magnitude *lower* than the materials can handle. The major concern for stress is the connector/rivet on the hinge. Using a half-hinge design (for a clean look below and ease of installation) with a soft-set rivet with a larger (0.375”) washer should distribute the stress sufficiently. Charlie and I verified this empirically by applying some rivets to the PMMA on Monday.

Here’s a link to my simulation setup:

https://drive.google.com/file/d/10gmb4aHjvcAB2MC4naD9hIOznVzsCTDn/view

Comments and criticisms welcome — I am hardly a Solidworks simulation expert and had to do a lot of reading to figure out how what I was trying to simulate. Unfortunately I couldn’t find an existing analytical solution for a triangular plate with a single clamped edge, and the derivation is beyond my skills in one day, so I had to go the simulation route.

### Follow-up Simulation Results

I went back to the simulations to try to understand that strange result I mentioned in class today:
  - With a two-sided hinge, the maximum stress on the PMMA was under the hinge, at 9.6MPa;
  - With a one-sided hinge with a large washer, the max stress under the washer was 8.5MPa and the max stress under the hinge was 21MPa.

The reason this was puzzling was: why is the max stress under the hinge on the one-sided design higher than the sum of the max stress under the hinge of the two-sided design and the max stress under the washer?

There are two reasons:
  - In the two-sided hinge design, the hinge is rigid enough that the preload of the rivet is insufficient to deform it to transfer the stress to the PMMA. Put another way, it is not actually clamping the PMMA. In the one-sided hinge design, the preload of the rivet is actually being transferred to the PMMA.
  - In the one-sided design, the max stress under the hinge is greater than the max stress under the washer because of where the stress is located. I do not understand why, but the simulation shows that the washer distributes its stress along the edge, while in the hinge there is a concentration of stress at the corner of the hole.

We walked through the simulations and looked at the stresses. The important thing we figured out is why, for the single sided hinge design, the stress is greatest at the corner of the bolt hole under the hinge. The reason is that the bolt hole restricts how the stress cone from the rivet head can propagate: it can’t propagate into the bolt hole itself. So at the edge of the bolt hole on the wing, the stress is effectively propagating straight down and not spreading. 

This turns out to be a well known result. :/ There’s a discontinuity and so the stress lines congregate at the wall of the bolt hole. Usually people worry about the bolt failing, not the plates, so it’s not the first thing you find in discussion of bolted joints.

What this means is that if we want to reduce this max stress, the solution is to add a washer to the rivet head on top of the hinge, complementing the one on the bottom where the rivet rests directly on the acrylic. This spreads the preload stress of the rivet across a larger area on the hinge surface, such that the stress right around the bolt hole is lower. 




