\documentclass{article}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    pdftitle={Overleaf Example},
    pdfpagemode=FullScreen,
    }
\usepackage{array}
\usepackage{indentfirst}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{float}
\usepackage{caption}
\usepackage{sectsty}
\subsubsectionfont{\fontsize{12}{15}\selectfont}
\subsubsectionfont{\fontsize{11}{15}\selectfont}

\input{structure.tex} % Include the file specifying the document structure and custom commands

%----------------------------------------------------------------------------------------
%	ASSIGNMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{Hardware and Preliminary Assembly Instructions for Mounting Mechanisms} % Title of the assignment

\author{Dea Dressel \\
{\tt\small ddressel@stanford.edu}
\and
Ava Jih-Schiff\\
{\tt\small avaj@stanford.edu}} % Author name and email address


\date{Stanford University --- \today} % University, school and/or department name(s) and a date

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\section*{Introduction} % Unnumbered section

This document outlines the hardware used in the initial prototypes of the mounting approaches as well as the requirements they satisfied. Additionally, it covers the preliminary assembly instructions of the prototypes for each mounting location.

%----------------------------------------------------------------------------------------
%	Section 1
%----------------------------------------------------------------------------------------

\section{Hardware}

The main criteria in choosing hardware was keeping a consistent thread size and material type. Most of the mounting parts were offered in low-strength, medium-strength, or zinc-plated steel. The indoor environment eliminated the need to use materials known for their anti-corrosion properties. Additionally, by using the same metal throughout, there is no risk of metal-to-metal corrosion. Some hardware parts had additional requirements which are described below.

\subsection{\href{https://www.e-rigging.com/one-eighth-X-200-foot-Coated-Galvanized-Cable}{Vinyl Coated Galvanized Cable}}
$\frac{1}{8}$" 7x19 vinyl coated galvanized cable was chosen for it's strength and load capacity. The vinyl coating was necessary for additional protection against rubbing and/or scratching the top beam of the building. One note is that the coating must be removed or peeled away where the aluminum sleeves are crimped to ensure maximum efficacy of the crimping.\\\\
Part Number: 21105020\\
\begin{center}
    \includegraphics[width=0.4\textwidth]{Cable.png}
\end{center}

\subsection{\href{https://www.e-rigging.com/one-eighth-inch-Sleeve}{Aluminum Sleeve}}

The crimping sleeve guidelines provided on McMaster-Carr's website dictated that aluminum sleeves will not corrode galvanized cable (as they would on stainless steel cable). Since the installation is indoors, there wasn't a need for weather proofing either. With these minimal requirements, the aluminum sleeves were the cheapest option.\\\\
Part Number: 62123031\\
\begin{center}
    \includegraphics[width=0.4\textwidth]{Crimping-Sleeve-1.png}
    \includegraphics[width=0.4\textwidth]{Crimping-Sleeve-2.png}
\end{center}

\subsection{\href{https://www.e-rigging.com/one-eighth-inch-Light-Duty-Wire-Rope-Thimble}{Wire Rope Thimbles}}

Thimbles were added to the design of the mount to avoid crimping and sharp bends in the cable. Should the flyer be hung on the cable without a thimble, the weight would become a point load and greatly increase the chances of fraying and breakage due to the increased stress. Our thimbles satisfy the zinc-plated steel choice outlined above and were cheaper than the stainless steel option.\\\\
Part Number: 51202031\\
\begin{center}
    \includegraphics[width=0.4\textwidth]{Thimble-1.png}
    \includegraphics[width=0.4\textwidth]{Thimble-2.png}
\end{center}

\subsection{\href{https://www.e-rigging.com/three-sixteenths-inch-Snap-Link}{Electro Galvanized Spring Snap Links}}

The carabiners are made of zinc-plated steel, were cheaper than the stainless steel option, and the $\frac{3}{16}$" size fits through the thimble.\\\\
Part Number: 41305051\\
\begin{center}
    \includegraphics[width=0.4\textwidth]{Carabiner.png}
\end{center}


\subsection{\href{https://www.mcmaster.com/pipe-hangers/threaded-rod-mount-clamping-hangers-8/}{Clamping Hangers}}

The clamping hangers are made of zinc-plated steel. This specific clamping hanger is designed to fit our $\frac{1}{2}$" pipe and $\frac{1}{4}$"-20 threaded rod. Additionally, this part comes with the required closure bolt and nut.\\\\
Part Number: 3006T78\\

\begin{center}
    \includegraphics[width=0.3\textwidth]{Clamping-Hanger.jpg}
\end{center}

\subsection{\href{https://www.mcmaster.com/eyebolts/eyebolts-for-lifting-8/}{Eyebolt}}

The eyebolts are made of steel. This specific part has a $\frac{1}{4}$"-20 threaded shank that fits into the clamping hangers and a $\frac{3}{4}$" eye diameter that fits the thimbles.\\\\ 
Part Number: 3013T45\\
\begin{center}
    \includegraphics[width=0.4\textwidth]{Eyebolt.png}
\end{center}


\subsection{\href{https://www.mcmaster.com/beam-clamps/beam-clamps-for-threaded-rod-7/}{Beam Clamp}}

The beam clamps are made of zinc-plated steel. This clamp has a spot for a $\frac{1}{4}$"-20 threaded rod to be attached either parallel or perpendicular to the mounting location, eliminating the need for different clamps for the front window mounting mechanism. This specific clamp has a maximum allowed beam thickness of $\frac{7}{8}$", which is larger than our $\frac{3}{4}$" beam webs and flanges. Additionally, they are rated for 100 pounds, making them sufficient for our load.\\\\
Part Number: 3027T76
\begin{center}
    \includegraphics[width=0.3\textwidth]{Beam-Clamp-3.jpg}
    \includegraphics[width=0.3\textwidth]{Beam-Clamp-1.jpg}
    \includegraphics[width=0.3\textwidth]{Beam-Clamp-2.jpg}
\end{center}

\subsection{\href{https://www.mcmaster.com/threaded-rods/low-strength-steel-threaded-rods-9/thread-size~1-4-20/}{Threaded Rod - Staircase}}

The threaded rods are made of zinc-plated steel. These rods have a $\frac{1}{4}$"-20 threading. The 4" length was chosen to allow room for the light conduit under the stairs. Additionally, the rods have a tensile strength of 50,000 psi.\\\\
Part Number: 91565A558\\
\begin{center}
    \includegraphics[width=0.9\textwidth]{Threaded-Rods.jpg}
\end{center}

\subsection{\href{https://www.mcmaster.com/threaded-rods/low-strength-steel-threaded-rods-9/thread-size~1-4-20/}{Threaded Rod - Front Window}}

The only difference between the threaded rods used for the front window mounts versus the staircase is the different length required to reach the pipe. Based on the size of the T-beam and distance calculations, the beam clamp on the bottom flange needed a 2.5" threaded rod and the beam clamp on the horizontal web needed a 8" threaded rod. The material and requirements satisfied by these rods are identical to the ones describe above.\\\\
Part Number(2.5"): 91565A552\\
Part Number(8"): 91565A567

\subsection{\href{https://www.mcmaster.com/hex-nuts/medium-strength-steel-hex-nuts-grade-5/thread-size~1-4-20/}{Hex Nuts}}
The hex nuts chosen are made of zinc-plated steel. These nuts have a $\frac{1}{4}$"-20 threading.\\
Part Number: 95462A029\\
\begin{center}
    \includegraphics[width=0.3\textwidth]{Hex-Nut.jpg}
\end{center}


%----------------------------------------------------------------------------------------
%	Section 2
%----------------------------------------------------------------------------------------

\section{Assembly}

\subsection{Ceiling}

The first mounting location is the circular shafts that extend across the Packard stairwell ceiling.\\

\begin{center}
    \includegraphics[scale=0.45]{ceiling-mechanism.png} %PHOTO YET TO EXIST
\end{center}\\

The ceiling mounting mechanism can be broken down into two core components: beam unit and vertical unit.\\

\noindent\textbf{Beam Unit}\\

The beam unit consists of:\\

\begin{minipage}[c]{0.65\linewidth}
\begin{description}\begin{itemize}
    \item 2.5 ft of $\frac{1}{8}$", 7x19, Vinyl Coated Galvanized Cable 
    \item Two $\frac{1}{8}$" Aluminum Sleeves 
    \item Two $\frac{1}{8}$" Light Duty Wire Rope Thimbles
\end{itemize}\end{description}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.35\linewidth}
\includegraphics[scale=0.15]{ceiling-beam-unit.png} %PHOTO YET TO EXIST
\end{minipage}\\\\

The beam unit loops around the ceiling shafts. The ceiling shafts have a circumference of around 2.1ft, which is why we decided to do 2.5ft for this unit. The beam unit wire has both ends thimbled and crimped. During crimping, the assembler must strip back the vinyl coating on the wire cable. The aluminum crimping sleeves will not work properly if this is not done correctly. \\

\noindent\textbf{Vertical Unit}\\

The vertical unit consists of:\\

\begin{minipage}[c]{0.65\linewidth}
\begin{description}\begin{itemize}
    \item Variable Length of $\frac{1}{8}$", 7x19, Vinyl Coated Galvanized Cable 
    \item Two $\frac{1}{8}$" Aluminum Sleeves 
    \item Two $\frac{1}{8}$" Light Duty Wire Rope Thimbles
    \item Two $\frac{3}{16}$" Electro Galvanized Spring Snap Links
\end{itemize}\end{description}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.35\linewidth}
\includegraphics[scale=0.3]{ceiling-vertical-unit.png} %PHOTO YET TO EXIST
\end{minipage}\\\\

The vertical unit extends vertically down from the beam unit. The vertical unit wire also has both ends thimbled and crimped. During crimping, the assembler must strip back the vinyl coating on the wire cable. The crimping sleeves will not work properly if this is not done correctly. The first carabiner clips both ends of the beam unit to the top end of the vertical unit. The second carabiner clips the bottom end of the vertical section to the fractal flyer. 

\subsection{Staircase}

The second mounting location is the I-beam on the inner and outer edges of the staircase.\\

\begin{center}
    \includegraphics[scale=0.15]{Staircase-Assembly.png} %PHOTO YET TO EXIST
\end{center}\\


The staircase mounting mechanism can be broken down into three core components: the clamping units, the hanging unit, and the wire unit.\\

\noindent\textbf{Clamping Units}\\

The staircase mounting mechanism has two clamping units that are assembled identically. Each clamping unit consists of:

\begin{minipage}[c]{0.65\linewidth}
\begin{description}\begin{itemize}
    \item One Beam Clamp for $\frac{1}{4}$" Threaded Rod  \\
    \item One $\frac{1}{4}$" Threaded Rod, 4" in length  \\
    \item One $\frac{1}{4}$" Threaded-Rod-Mount\\Clamping Hanger    \\
    \item Five $\frac{1}{4}$" Steel Hex Nuts   \\
\end{itemize}\end{description}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.35\linewidth}
\includegraphics[scale=0.04]{staircase-clamping-unit.png}
\end{minipage}\\\\

The top end of the threaded rod goes through the vertical threaded hole in the beam clamp, secured with a hex nut on both sides of the beam clamp. The bottom end of the threaded rod passes through the hole in the top face of the clamping hanger, secured by hex nuts on both sides of the clamping hanger top face. A hex nut is screwed onto the lock bolt of the beam clamp to allow for tightening to the I-beam. 

The clamping hangers clamp around the 1/2" pipe, tightened by screwing the hex nut on the closer bolt. The first clamping unit is 1" from the end of the beam with the beam clamp facing inward. The second clamping unit is 7" apart from the first clamping unit, for a total of 8" from the end of the beam. The second clamping unit faces outward. The two clamping units "face" each other, as seen below.

\begin{center}
    \includegraphics[scale=0.1]{staircase-clamping-units.png}
\end{center}\\

To ensure that all hex nuts are properly tightened, use a wrench. For the clamping hanger closer bolts, you will need a wrench and a screwdriver to keep the bolt still.\\


\noindent\textbf{Hanging Unit}\\

The staircase mounting mechanism has one hanging unit. A hanging unit consists of: \\

\begin{minipage}[c]{0.65\linewidth}
\begin{description}\begin{itemize}
    \item One $\frac{1}{4}$" Threaded-Rod-Mount Clamping Hanger
    \item One $\frac{3}{4}$" Eyebolt
    \item Two $\frac{1}{4}$" Steel Hex Nuts
\end{itemize}\end{description}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.35\linewidth}
\includegraphics[scale=0.6]{staircase-hanging-unit.png} %PHOTO DOESN'T EXIST YET
\end{minipage}\\

The eyebolt threads pass through the hole in the top face of the clamping hanger, secured by two hex nuts on both sides of the top face. The hanging unit is attached to the steel pipe such that the eyebolt, from which the fractal flyer hangs, is oriented downwards. The hanging unit is spaced 2" from the far end of the pipe (opposite end from where the clamping units are located).\\

\noindent\textbf{Wire Unit}\\

The staircase mounting mechanism has one wire unit. A wire unit consists of:\\


\begin{minipage}[c]{0.65\linewidth}
\begin{description}\begin{itemize}
    \item Variable length of $\frac{1}{8}$", 7x19, Vinyl Coated Galvanized Cable
    \item Two $\frac{1}{8}$" Aluminum Sleeves 
    \item Two $\frac{1}{8}$" Light Duty Wire Rope Thimbles
    \item One $\frac{3}{16}$" Electro Galvanized Spring Snap Links
\end{itemize}\end{description}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.35\linewidth}
\includegraphics[scale=0.3]{staircase-wire-unit.png} %PHOTO DOESN'T EXIST YET
\end{minipage}\\

Similar to the vertical unit in the ceiling mounting mechanism, this section of wire has both ends thimbled and crimped. The length of the wire unit is variable. When choosing the length of this unit, pay attention to the distance the fractal flyer has to any surrounding walls, windows or other flyers. 

The top end loop of the vertical unit should be thimbled and crimped around the eyebolt during bench assembly. This means that the wire unit should already be attached to the hanging unit before the hanging unit is clamped around the pipe. The carabiner clips the bottom end loop of the wire unit to the fractal flyer. 


\subsection{Front Window}

The third mounting location is the horizontal T-beams on the front windows in the stairwell. 

\begin{center}
    \includegraphics[scale=0.3]{Front-Window-Mechanism.png} %PHOTO YET TO EXIST
\end{center}\\


The front window mounting mechanism can be broken down into three core components: clamping units (web clamping unit \& bottom flange clamping unit), the hanging unit, and the wire unit.\\

\noindent\textbf{Clamping Units}\\

\item \noindent\textbf{1. Bottom Flange Clamping Unit}\\

Each bottom flange clamping unit consists of:\\

\begin{minipage}[c]{0.65\linewidth}
\begin{description}\begin{itemize}
    \item One Beam Clamp for $\frac{1}{4}$" Threaded Rod  \\
    \item One $\frac{1}{4}$" Threaded Rod, 2.5" in length  \\
    \item One $\frac{1}{4}$" Threaded-Rod-Mount\\Clamping Hanger    \\
    \item Five $\frac{1}{4}$" Steel Hex Nuts   \\
\end{itemize}\end{description}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.35\linewidth}
\includegraphics[scale=0.08]{front-window-bottom-flange-clamping-unit.png} 
\end{minipage}\\


The top end of the threaded rod goes through the horizontal threaded hole in the beam clamp, secured with a hex nut on both sides of the beam clamp. The inner hex nut does not fit into the beam clamp crevice. This requires the assembler to ensure that the hex nut and top end of the threaded rod do not interfere with the clamp's lock nut's path. The bottom end of the threaded rod passes through the hole in the top face of the clamping hanger, secured by hex nuts on both sides of the clamping hanger top face. A hex nut is screwed onto the lock bolt of the beam clamp to allow for tightening to the bottom flange. \\

\noindent\textbf{2. Web Clamping Unit}\\

Each web clamping unit consists of:

\begin{minipage}[c]{0.65\linewidth}
\begin{description}\begin{itemize}
    \item One Beam Clamp for $\frac{1}{4}$" Threaded Rod  \\
    \item One $\frac{1}{4}$" Threaded Rod, 8" in length  \\
    \item One $\frac{1}{4}$" Threaded-Rod-Mount\\Clamping Hanger    \\
    \item Five $\frac{1}{4}$" Steel Hex Nuts   \\
\end{itemize}\end{description}
\end{minipage} % no space if you would like to put them side by side
\begin{minipage}[c]{0.35\linewidth}
\includegraphics[scale=0.06]{front-window-web-clamping-unit.png} 
\end{minipage}\\

The top end of the threaded rod goes through the vertical threaded hole in the beam clamp, secured with a hex nut on both sides of the beam clamp. The bottom end of the threaded rod passes through the hole in the top face of the clamping hanger, secured by hex nuts on both sides of the clamping hanger top face. A hex nut is screwed onto the lock bolt of the beam clamp to allow for tightening to the T-beam web. \\

\begin{center}
    \includegraphics[scale=0.1]{front-window-clamping-units.png}
\end{center}\\

The clamping hangers clamp around the 1/2" pipe, tightened by screwing the hex nut on the closer bolt. The bottom flange clamping unit is 1" from the end of the beam with the beam clamp facing upward. The web clamping unit is 5" apart from the web clamping unit, for a total of 6" from the end of the beam (as shown in figure above). This distance was an estimate of the length of the front window t-beam web (it should be remeasured).\\

\noindent\textbf{Hanging Unit}\\

Refer to the hanging unit portion of the staircase subsection above.\\

\noindent\textbf{Wire Unit}\\

Refer to the wire unit portion of the staircase subsection above.


%------------------------------------------------



% \begin{description}\begin{enumerate}
%   \item BEAM CLAMPSSSS
%   \item 
%   \item 
  
% \end{enumerate}\end{description}

% \begin{figure}[H]
%     \includegraphics[width=0.9\textwidth]{Ceiling-Option.png}
%     \caption{Ceiling Mounting Design}
% \end{figure}

%------------------------------------------------

%  \begin{tabular}{ll}
%      \textbf{Cable:}  & $\$1.56$ for 8'\\ 
%      \textbf{Crimping Sleeves:}  & $\$0.10$ x 4\\ 
%      \textbf{Thimble:}  & $\$0.20$ x 4\\ 
%      \textbf{Carabiner Clip:}  & $\$0.40$ x 2\\ 
%      \rule{120}{1} & \rule{39}{1}\\ 
%      \textbf{Total Cost per Mount:}  & $\$3.56$\\ 
%  \end{tabular}

\end{document}