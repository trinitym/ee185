# Mounting Approaches

This directory stores all of the files, notes, and techincal documents for
the mounting approaches used to attach the Fractal Flyers to the Packard
stairwell.

The directory structure:
  - design-docs: documentation of design decisions and technical components of the mounting approaches,
  - hardware: documentation of the hardware purchased for each mounting approach,
  - solidworks: CAD parts and assemblies of mounting approaches for staircase and front window, and displacement results from simulations