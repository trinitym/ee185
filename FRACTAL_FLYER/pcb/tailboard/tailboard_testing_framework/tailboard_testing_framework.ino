#include <Adafruit_NeoPixel.h>


#define LED1_SIG 13
#define LED2_SIG 12
#define LED3_SIG 11

#define LED_COUNT 230
#define WING_LED_COUNT 75
#define DUMMY_EN A3

#define BRIGHTNESS 83

// the setup function runs once when you press reset or power the board
#define IN_1B 9
#define IN_2B 10
#define IN_1A 5
#define IN_2A 6
#define LED1_EN A4
#define LED2_EN 4
#define LED3_EN A5

float percentage = 1;
bool direction = 0;

Adafruit_NeoPixel strip_1(WING_LED_COUNT, LED1_SIG, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip_2(LED_COUNT, LED2_SIG, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip_3(WING_LED_COUNT, LED3_SIG, NEO_GRB + NEO_KHZ800);
static uint16_t hue = 0;


void rainbowFade2White(int wait, int rainbowLoops, int whiteLoops) {
  int fadeVal=100, fadeMax=100;

  // Hue of first pixel runs 'rainbowLoops' complete loops through the color
  // wheel. Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to rainbowLoops*65536, using steps of 256 so we
  // advance around the wheel at a decent clip.
  for(uint32_t firstPixelHue = 0; firstPixelHue < rainbowLoops*2000;
    firstPixelHue += 256) {

    for(int i=0; i<strip_1.numPixels(); i++) { // For each pixel in strip...

      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      uint32_t pixelHue = firstPixelHue + (i * 65536L / strip_1.numPixels());

      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the three-argument variant, though the
      // second value (saturation) is a constant 255.
      strip_1.setPixelColor(i, strip_1.gamma32(strip_1.ColorHSV(pixelHue, 255,
        255 * fadeVal / fadeMax)));
    }

    for(int i=0; i<strip_2.numPixels(); i++) { // For each pixel in strip...

      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      uint32_t pixelHue = firstPixelHue + (i * 65536L / strip_2.numPixels());

      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the three-argument variant, though the
      // second value (saturation) is a constant 255.
      strip_2.setPixelColor(i, strip_2.gamma32(strip_2.ColorHSV(pixelHue, 255,
        255 * fadeVal / fadeMax)));
    }

    for(int i=0; i<strip_3.numPixels(); i++) { // For each pixel in strip...

      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      uint32_t pixelHue = firstPixelHue + (i * 65536L / strip_3.numPixels());

      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the three-argument variant, though the
      // second value (saturation) is a constant 255.
      strip_3.setPixelColor(i, strip_3.gamma32(strip_3.ColorHSV(pixelHue, 255,
        255 * fadeVal / fadeMax)));
    }
    strip_1.show();
    strip_2.show();
    strip_3.show();
    delay(wait);
    }
}

void setup() {
  // initialize digital pin 13 as an output.
  Serial.begin(9600);
  pinMode(IN_1B, OUTPUT);
  pinMode(IN_2B, OUTPUT);
  pinMode(IN_1A, OUTPUT);
  pinMode(IN_2A, OUTPUT);
  pinMode(DUMMY_EN, OUTPUT);
  pinMode(LED1_EN, OUTPUT);
  pinMode(LED2_EN, OUTPUT);
  pinMode(LED3_EN, OUTPUT);

  digitalWrite(DUMMY_EN, LOW);

  strip_1.begin();
  strip_1.show();
  strip_1.setBrightness(BRIGHTNESS);

  strip_2.begin();
  strip_2.show();
  strip_2.setBrightness(BRIGHTNESS);

  strip_3.begin();
  strip_3.show();
  strip_3.setBrightness(BRIGHTNESS);
  //static uint16_t hue = 0;
}

// the loop function runs over and over again forever
void loop() {
  if (Serial.available() > 0) {
    char command;
    Serial.readBytes(&command, 1);

    switch (command) {
      case 'm':
        delay(1000);
        if (Serial.available() > 0) percentage = Serial.readString().toFloat();
        break;
      case '1':
        digitalWrite(LED1_EN, !digitalRead(LED1_EN));
        break;
      case '2':
        digitalWrite(LED2_EN, !digitalRead(LED2_EN));
        break;
      case '3':
        digitalWrite(LED3_EN, !digitalRead(LED3_EN));
        break;
      case '4':
        digitalWrite(DUMMY_EN, !digitalRead(DUMMY_EN));
        break;
      case '5':
        direction = 0;
        break;
      case '6':
        direction = 1;
        break;
      case '7':
        rainbowFade2White(3, 3, 1);
        break;
      case 's':
        percentage = 1;
        break;
  }

  if (direction) {
    pinMode(IN_1B, OUTPUT);
    pinMode(IN_2B, OUTPUT);
    pinMode(IN_1A, OUTPUT);
    pinMode(IN_2A, OUTPUT);

    analogWrite(IN_2A, 255*percentage);
    digitalWrite(IN_1A, 1);

    analogWrite(IN_1B, 255*percentage);
    digitalWrite(IN_2B, 1);
  } else {
    pinMode(IN_1B, OUTPUT);
    pinMode(IN_2B, OUTPUT);
    pinMode(IN_1A, OUTPUT);
    pinMode(IN_2A, OUTPUT);
    analogWrite(IN_1A, 255*percentage);
    digitalWrite(IN_2A, 1);

    analogWrite(IN_2B, 255*percentage);
    digitalWrite(IN_1B, 1);
  }

 // static uint16_t hue = 0; // Starting hue value

  // Shift the color wheel
}
}