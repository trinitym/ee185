# Testboard V2

## Quick Reference
Firmware location: software/scripts/testboard-v2.py
PCB location: current directory

## Context

The testboard is used to test LED strips.

Testboard-V2 (Designed Spring 2022) adds digital line protection in case the LED strip digital pin is shorted. Also adds in some other failsafes to ensure that any nonfunctioning LED strip is due to the strip and not an issue with the testboard. 

A small footprint rework allows for two LEDs to be plugged in at the same time

Please reference [this](/FRACTAL_FLYER/pcb/testboard-v2/Test%20Board%20Design%20Doc%20V2.pdf) for full design spec.
