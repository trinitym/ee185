# FLIGHT: Packard Interactive Light Sculpture Project

This repository stores all of the files, notes, and technical documents
for FLIGHT, the interactive light sculpture for the Packard building at
Stanford Unviersity.  Stanford students
taking EE285 and CS241, under guidance from course instructors,
engineer an art piece designed by Charles Gadeken. This engineering involves
designing, building, and testing the mechanical, electrical, and software
components of the pieces.

![GUI](images/gui.png) ![Top](images/top.png) ![bottom](images/bottom.png)

This git repository depends on a few submodule for supporting software
and tools. After you clone this repository, go to the root of the
repository and type

```
$ git submodule init
$ git submodule update
```

This will clone the submodules for the DigiKey KiCAD library (needed
for the Fractal Flyer printed circuit board) and for Heronart's
LXStudio package (needed for the GUI that controls and animates
the simulation).

The directory structure:
  - FlightDesign.pdf: a top-level design document, which describes the major elements of FLIGHT, their design considerations, how they work, and some background on why particular approaches are used.
  - FlyerConstruction.pdf: a complete assembly guide for a Fractal Flyer.
  - FRACTAL\_FLYER: all technical information on Fractal Flyers, the moving, illuminated objects that form FLIGHT. 76 Fractal Flyers are hung in Packard to form the complete FLIGHT installation.
  - software: the software to control and visualize FLIGHT, including firmware, UI, and simulation.
  - packard: architectural drawings and models of Packard, including its stairwell
  - course-materials: materials from course offerings of the course, including assignments and project reports

Further resources:
  - [Fractal Flyer Bill of Materials](https://docs.google.com/spreadsheets/d/17VOOdsOzH8AatJcCMNQgFOtFsnBo5ioCs1vLwKEDTtI/edit?usp=sharing)
  - [Fractal Flyer CAD](https://cad.onshape.com/documents/1658552cb3cf9b97eb4187b0/w/db1b832a8e6a42671155c1fd/e/7ad09a5b999f18148690839f)
  - [Fractal Flyer Drawing Tree (out of date)](https://docs.google.com/document/d/1t9YB293jo6p6bc5lDQ5S1L3p2o_mtnMwk6ygtS_yizA/edit)

