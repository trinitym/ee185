# Fractal Flyer Firmware  

This directory contains the Fractal Flyer firmware, which runs on the Feather M4 on the tailboard. The firmware is a custom build of [CircuitPython](https://circuitpython.org/) which incorporates an interrupt-based PID control loop for the wings as well as custom CircuitPython bindings (C firmware functions callable from Python) to control the wings and LEDs. This directory contains
  - circuitpython-main: the firmware (details below)
  - doc: reports on the firmware and its design

## Introduction

Our goal is to create a complete firmware image that can allow us to control each Flyer in a simple and predictable way. At at high level, this means making an interface between the FlightGUI and Flyer firmware, building the backend to that firmware and then ensuring that the backend holds up against possible external faults for the sake of safety and longevity. Each Flyer also has a locally running main script that executes when the FlightGUI is not sending commands. This also will be built on the firmware. 

This directory stores source code for the firmware that runs on a Fractal Flyer.

A FractalFlyer has a [Stanford Maxwell](https://github.com/maholli/sam32) board, 
which is a variant of the [Feather M4 Express
board](https://www.adafruit.com/product/3857). 

The firmware provides a [CircuitPython](https://circuitpython.org/)-based programming interface for which we are building custom modules and bindings to interact with the Flyer.

## API

Interactive control of Flyers will be conducted using a combination of these simple functions:

body_leds((uint8_t r, uint8_t g, uint8_t b))
* Defines the color of the body of the Flyer

wing_leds(uint8_t left, (r1,g1,b1),(r2,g2,b2),(r3,g3,b3))
* if left is 1 the left pin is used, otherwise the right pin is; see module for details
* defines the colors for a wing's LEDs
* the wing LEDs are to be patterned in a gradient so we have to send 3 RGB values to define the 3 corners of the gradient on the wing

wing_angle(int angle, bool side)
Sets a new setpoint for the motor control loop

## Getting Started

### CircuitPython One-Time Setup

First, clone the broader `EE185` repository to your local machine. If you are unable to clone this repo, you may need to set up an SSH key for your machine and/or get Phil to upgrade your account permissions. The folder circuitpy-main is a clone of the 6.1.x branch of [CircuitPython](https://github.com/adafruit/circuitpython), but with all the submodules initialized and the .git folder removed. Translation: this folder has everything you need to build CircuitPython minus a few external packages.

The Feather M4 Express boards we are using come preloaded with CircuitPython. In order to build and deploy firmware, you will also need to set up your local environment with some external dependencies according to your OS. 

#### Mac

What things you need to install the software and how to install them

#### Windows

What things you need to install the software and how to install them

#### Linux

With a standard Linux distro, the only external dependency you should need is arm-none-eabi-gcc, which sometimes comes preinstalled. Follow the steps outlined at https://learn.adafruit.com/building-circuitpython/linux to make sure you have everything you need. The link should also provide assistance for other Linux distros and other possible issues. 

Some distribution of linux may not automatically mount storage devices, see documentation for your distribution if this is the case for you.


### Building CircuitPython

`mpy-cross` compiles .py files into .mpy files. It needs to be built locally, usually just once, before you can build CircuitPy. In the circuitpython-main folder, run

```
make -C mpy-cross
```

Then, to build firmware for the Feather M4 Express board, enter the `ports/atmel-samd` directory and run
```
make -j8 BOARD=feather_m4_express
```
There are many other boards that we can build firmware for using CircuitPy. Check `ports/atmel-samd/boards` for a full list.

The -j flag tells make how many independent jobs/threads to run, so you can adjust the number depending on your hardware.

Note: if you get an error "failed to make bitstream", you may have forgotten to make mpy-cross. If you attempt to make the firmware without mpy-cross, you will need to run `make clean` first before attempting to build again. 


### Deploying Firmware to the Feather M4 Express

To deploy firmware to your Feather M4 Express:

1. Build a new firmware image and navigate to `ports/atmel-samd/build-feather_m4_express/`.
2. Plug in the Feather M4 board to your computer using a **data** microUSB cable. 
3. Press the reset button twice to enter Bootloader mode. The LED on the board should flash green in this mode. You should also see a storage device with name ending in BOOT on your computer, much like a flash drive. 
4. Copy the firmware image you built located at, named `firmware.uf2` to the storage device
5. Do **not** unplug or reset the Feather M4 after uploading a new firmware image! The board will reboot itself and reappear on your machine in its standard mode (usually "CIRCUITPY"). It is now running your newly updated firmware! 


# Implementation

There are two modules added to implement the circuitpython extensions: ee285 and control. Each module requires a partner bindings file to connect its functionality to the API Python commands.

The ee285 module handles both LED interpolation and binding motor control loop functionality to Python. Relevant directories and files are:

/shared-bindings/ee285 
/shared-modules/ee285 

The control module handles the implementation of the motor control loop, sensor reading, and pwm output. Relevant directories and files are: 

/ports/atmel-samd/common-hal/motor_control - heart of motor control loop functionality

SPI - SPI driver. Initializes the SPI bus and allows for reading + writing bytes from it

I2C - I2C driver. Initializes the I2C bus and allows for reading + writing bytes from it

Accel - driver for the LIS3DH accelerometer. Based on the Adafruit LIS3DH library in Python. Includes all functions necessary to    initalize an accelerometer with SPI or I2C, read data from it and reset it

PID - where all control loop logic is implemented. Contains functions to get wing angles and set PWM duty cycles for the motors based on a PI controller

PWM - takes output from PID.c and sends duty cycles to the motors via an H-bridge connection

Control - defines functions to initialize the timer for interrupts, enable interrupts and the interrupt handler that controls the whole motor control loop


 /shared-bindings/motor_control -- OBSOLETE, functionality moved to shared-bindings/ee285/__init__.c
 /shared-modules/motor_control -- OBSOLETE, functionality moved to shared-modules/ee285/__init__.c


## Built With

* [CircuitPython](https://circuitpython.readthedocs.io/en/6.0.x/README.html) - The (small) Python library on the Feather M4
* [NeoPixel](https://github.com/adafruit/Adafruit_CircuitPython_NeoPixel) - The CircuitPython library for communicating with the LED strips

## Creating C Modules in CircuitPython
1.  Follow the guide [here](https://learn.adafruit.com/extending-circuitpython/inside-the-virtual-machine) until it gets to the files in /ports/atmel-samd
2.  Edit py/circuitpy_mpconfig.h instead of ports/atmel-samd, and copy the format of the other modules where all the externs and modules are defined, as well as the MICROPY_PORT_BUILTIN_MODULES_STRONG_LINKS macro
3.  Edit py/circuitpy_defns.mk  instead of the Makefile, and follow the pattern with the other modules: add the module name to SRC_PATTERNS, and the c files to SRC_SHARED_MODULE_ALL


## Authors

* **Abby Audet** 
* **Albert** 
* **Akwasi**
* **Vidisha Srivastav** 

