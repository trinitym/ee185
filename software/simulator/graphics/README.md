FlightSimulator is the Unity 5 project which displays the graphics of the flight simulator. You can customize the number of flyers being spawned by changing the parameter under the Packard Staircase model. The codebase is currently being developed to connect with the existing fsimulator project.

**Installing Unity Hub**

Go to https://store.unity.com/

-> Click the 'Individual' tab

-> Click 'Sign Up' underneath the 'Student' Plan (if this doesn't work, you can also try the 'Personal' plan; both are free)

---> For the 'Student' plan, you will need to follow the steps to sign up with your Github account (which needs to be verified with your school email address + apply for the Student Developer Pack, which may take a few weeks if you have not already done so) and then activate your Unity Student plan

---> If instead you choose to go with the 'Personal' plan, click "Get Started"

---> Click "Start Here" to download the Unity Hub Installer with a self-guided learning path + "Go Here", if you just want the vanilla Unity Installer

---> If the above instructions do not work, you can go directly to this link to download the Unity Hub Installer for OSX: https://store.unity.com/download/thank-you?thank-you=personal&os=osx&nid=1815 or for Windows: https://store.unity.com/download/thank-you?thank-you=personal&os=win&nid=1815 -- There are also great resources to get started learning Unity here

-> Open up the installer + click "Agree" to agree with the terms and conditions

**Installing Unity**

-> When the installer loads, click "Sign In" to add your license

-> Go to "Installs" on the left sidebar, click "Add" (or "Locate" if you already have a version of Unity downloaded separately) and select the latest "Recommended Release"

-> No modules need to be added, so you can click "Done"

**Installing the FlightSimulator Project**

->Once you are done, you can go to "Projects" on the left sidebar, click "Add" and find the "FlightSimulator" project where you have saved it locally on your computer

->You should not have to select a Unity version or target platform, as those should be auto-configured, but if you are running a version of Unity that is different than the version used to build the project, you will have to go in the column marked "Unity Version" or "Target Platform", and click on the dropdown menu for the FlightSimulator project, and select the version/platform specific to your computer (version should be the latest version, which is already on your computer, + current platform)

-> doing this may result in warnings, especially if you are downgrading in version

-> we want to keep this as up-to-date as possible, so if your Unity version is newer than the project, please upgrade the project, accepting all the warnings about the possibility of data loss, and after making sure the project runs properly, upload the updated vision to Gitlab

**Running the Project**

-> Select FlightSimulator in the list + Unity should open up (it will take some time to load the first time you open it)

-> Click "Play" to watch the animation and make sure the default script works

**Modified flyer models for FlightSimulator**

We have used modified models for the flyer, so to access those, please find them at:

Overall Flyer Model (pared down)

https://cad.onshape.com/documents/1c416b952e91edf6a129aa58/w/894b2f14debd97d76e3c797b/e/9fd715f958a6e04e5e75292d

Left wing (from top) w/ re-aligned origin:

https://cad.onshape.com/documents/eb25ac52e2490863f63ecfa8/w/2205a64845533321fe643c61/e/8732571372ccb2c0216afbe2

Right wing (from top) w/ re-aligned origin:

https://cad.onshape.com/documents/9e71dea114ad6b7833e40d03/w/c8241e071e1c6ca9d52dae04/e/d3d08f8426b401549f6d5315

**Helpful Links**

Navigation:
https://docs.unity3d.com/2019.3/Documentation/Manual/SceneViewNavigation.html

Basic Unity Scripting:

https://docs.unity3d.com/2019.3/Documentation/Manual/ScriptingSection.html

Unity Scripting API:

https://docs.unity3d.com/2019.3/Documentation/ScriptReference/index.html
