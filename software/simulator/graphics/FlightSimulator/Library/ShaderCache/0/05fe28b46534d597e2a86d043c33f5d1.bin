<Q                           #  #ifdef VERTEX
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 unity_WorldTransformParams;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 unity_LightmapST;
uniform 	vec4 unity_DynamicLightmapST;
uniform 	bvec4 unity_MetaVertexControl;
uniform 	vec4 _MainTex_ST;
uniform 	vec4 _Mask_ST;
uniform 	vec4 _Noise_ST;
uniform 	vec4 _BumpMap_ST;
in  vec4 in_POSITION0;
in  vec4 in_TANGENT0;
in  vec3 in_NORMAL0;
in  vec4 in_TEXCOORD0;
in  vec4 in_TEXCOORD1;
in  vec4 in_TEXCOORD2;
out vec4 vs_TEXCOORD0;
out vec4 vs_TEXCOORD1;
out vec4 vs_TEXCOORD2;
out vec4 vs_TEXCOORD3;
out vec4 vs_TEXCOORD4;
vec4 u_xlat0;
bool u_xlatb0;
vec4 u_xlat1;
vec3 u_xlat2;
vec3 u_xlat3;
float u_xlat12;
bool u_xlatb12;
void main()
{
    u_xlatb0 = 0.0<in_POSITION0.z;
    u_xlat0.z = u_xlatb0 ? 9.99999975e-05 : float(0.0);
    u_xlat0.xy = in_TEXCOORD1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
    u_xlat0.xyz = (unity_MetaVertexControl.x) ? u_xlat0.xyz : in_POSITION0.xyz;
    u_xlatb12 = 0.0<u_xlat0.z;
    u_xlat1.z = u_xlatb12 ? 9.99999975e-05 : float(0.0);
    u_xlat1.xy = in_TEXCOORD2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
    u_xlat0.xyz = (unity_MetaVertexControl.y) ? u_xlat1.xyz : u_xlat0.xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD0.zw = in_TEXCOORD0.xy * _Mask_ST.xy + _Mask_ST.zw;
    vs_TEXCOORD1.xy = in_TEXCOORD0.xy * _Noise_ST.xy + _Noise_ST.zw;
    vs_TEXCOORD1.zw = in_TEXCOORD0.xy * _BumpMap_ST.xy + _BumpMap_ST.zw;
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat0.xyz = vec3(u_xlat12) * u_xlat0.xyz;
    u_xlat1.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].yzx;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].yzx * in_TANGENT0.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].yzx * in_TANGENT0.zzz + u_xlat1.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * u_xlat1.xyz;
    u_xlat2.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat2.xyz = u_xlat0.zxy * u_xlat1.yzx + (-u_xlat2.xyz);
    u_xlat12 = in_TANGENT0.w * unity_WorldTransformParams.w;
    u_xlat2.xyz = vec3(u_xlat12) * u_xlat2.xyz;
    vs_TEXCOORD2.y = u_xlat2.x;
    u_xlat3.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat3.xyz;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat3.xyz;
    u_xlat3.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat3.xyz;
    vs_TEXCOORD2.w = u_xlat3.x;
    vs_TEXCOORD2.x = u_xlat1.z;
    vs_TEXCOORD2.z = u_xlat0.y;
    vs_TEXCOORD3.x = u_xlat1.x;
    vs_TEXCOORD4.x = u_xlat1.y;
    vs_TEXCOORD3.z = u_xlat0.z;
    vs_TEXCOORD4.z = u_xlat0.x;
    vs_TEXCOORD3.w = u_xlat3.y;
    vs_TEXCOORD4.w = u_xlat3.z;
    vs_TEXCOORD3.y = u_xlat2.y;
    vs_TEXCOORD4.y = u_xlat2.z;
    return;
}

#endif
#ifdef FRAGMENT
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ColorRamp_ST;
uniform 	float _Blend;
uniform 	float _BumpPower;
uniform 	float _Distortion;
uniform 	float _Hue;
uniform 	float _Saturation;
uniform 	float _Brightness;
uniform 	float _Contrast;
uniform 	bvec4 unity_MetaFragmentControl;
uniform 	float unity_OneOverOutputBoost;
uniform 	float unity_MaxOutputValue;
UNITY_LOCATION(0) uniform  sampler2D _MainTex;
UNITY_LOCATION(1) uniform  sampler2D _Noise;
UNITY_LOCATION(2) uniform  sampler2D _BumpMap;
UNITY_LOCATION(3) uniform  sampler2D _Mask;
UNITY_LOCATION(4) uniform  sampler2D _ColorRamp;
in  vec4 vs_TEXCOORD0;
in  vec4 vs_TEXCOORD1;
in  vec4 vs_TEXCOORD2;
in  vec4 vs_TEXCOORD3;
in  vec4 vs_TEXCOORD4;
layout(location = 0) out vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
float u_xlat3;
float u_xlat4;
float u_xlat5;
float u_xlat15;
void main()
{
    u_xlat0 = texture(_BumpMap, vs_TEXCOORD1.zw);
    u_xlat0.x = u_xlat0.w * u_xlat0.x;
    u_xlat0.xy = u_xlat0.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    u_xlat15 = dot(u_xlat0.xy, u_xlat0.xy);
    u_xlat15 = min(u_xlat15, 1.0);
    u_xlat15 = (-u_xlat15) + 1.0;
    u_xlat15 = sqrt(u_xlat15);
    u_xlat0.z = u_xlat15 / _BumpPower;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat1.x = vs_TEXCOORD2.w;
    u_xlat1.y = vs_TEXCOORD3.w;
    u_xlat1.z = vs_TEXCOORD4.w;
    u_xlat1.xyz = (-u_xlat1.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * u_xlat1.xyz;
    u_xlat2.xyz = u_xlat1.yyy * vs_TEXCOORD3.xyz;
    u_xlat1.xyw = vs_TEXCOORD2.xyz * u_xlat1.xxx + u_xlat2.xyz;
    u_xlat1.xyz = vs_TEXCOORD4.xyz * u_xlat1.zzz + u_xlat1.xyw;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat2.xyz = vec3(u_xlat15) * u_xlat1.xyz;
    u_xlat1.xy = u_xlat1.xy * _ColorRamp_ST.xy;
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
    u_xlat2 = texture(_Noise, vs_TEXCOORD1.xy);
    u_xlat5 = u_xlat2.x * _Distortion;
    u_xlat0.xy = u_xlat0.xx * vec2(u_xlat5) + u_xlat1.xy;
    u_xlat0.xy = u_xlat0.xy + _ColorRamp_ST.zw;
    u_xlat0 = texture(_ColorRamp, u_xlat0.xy);
    u_xlat1 = texture(_Mask, vs_TEXCOORD0.zw);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat1.xyz = (-u_xlat1.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat2 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat1.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat0.xyz = max(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat1.xyz = u_xlat0.zxy * vec3(0.57735002, 0.57735002, 0.57735002);
    u_xlat1.xyz = u_xlat0.zxy * vec3(0.57735002, 0.57735002, 0.57735002) + (-u_xlat1.zxy);
    u_xlat15 = _Hue * 6.28318548;
    u_xlat3 = sin(u_xlat15);
    u_xlat4 = cos(u_xlat15);
    u_xlat1.xyz = u_xlat1.xyz * vec3(u_xlat3);
    u_xlat1.xyz = u_xlat0.xyz * vec3(u_xlat4) + u_xlat1.xyz;
    u_xlat0.x = dot(vec3(0.57735002, 0.57735002, 0.57735002), u_xlat0.xyz);
    u_xlat0.x = u_xlat0.x * 0.57735002;
    u_xlat5 = (-u_xlat4) + 1.0;
    u_xlat0.xyz = u_xlat0.xxx * vec3(u_xlat5) + u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz + vec3(-0.5, -0.5, -0.5);
    u_xlat15 = _Contrast + _Contrast;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat15) + vec3(0.5, 0.5, 0.5);
    u_xlat15 = _Brightness * 2.0 + -1.0;
    u_xlat0.xyz = vec3(u_xlat15) + u_xlat0.xyz;
    u_xlat15 = dot(u_xlat0.xyz, vec3(0.389999986, 0.589999974, 0.109999999));
    u_xlat0.xyz = (-vec3(u_xlat15)) + u_xlat0.xyz;
    u_xlat1.x = _Saturation + _Saturation;
    u_xlat0.xyz = u_xlat1.xxx * u_xlat0.xyz + vec3(u_xlat15);
    u_xlat0.xyz = (-u_xlat2.xyz) + u_xlat0.xyz;
    u_xlat0.xyz = vec3(vec3(_Blend, _Blend, _Blend)) * u_xlat0.xyz + u_xlat2.xyz;
    u_xlat0.xyz = log2(u_xlat0.xyz);
    u_xlat15 = unity_OneOverOutputBoost;
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat15);
    u_xlat0.xyz = exp2(u_xlat0.xyz);
    u_xlat0.xyz = min(u_xlat0.xyz, vec3(vec3(unity_MaxOutputValue, unity_MaxOutputValue, unity_MaxOutputValue)));
    u_xlat0.w = 1.0;
    u_xlat0 = (unity_MetaFragmentControl.x) ? u_xlat0 : vec4(0.0, 0.0, 0.0, 0.0);
    SV_Target0 = (unity_MetaFragmentControl.y) ? vec4(0.0, 0.0, 0.0, 1.0) : u_xlat0;
    return;
}

#endif
w                             $GlobalsX         _WorldSpaceCameraPos                         _ColorRamp_ST                           _Blend                        
   _BumpPower                    $      _Distortion                   (      _Hue                  ,      _Saturation                   0      _Brightness                   4   	   _Contrast                     8      unity_MetaFragmentControl                    @      unity_OneOverOutputBoost                  P      unity_MaxOutputValue                  T          $Globals@        unity_WorldTransformParams                    �      unity_LightmapST                  �      unity_DynamicLightmapST                   �      unity_MetaVertexControl                  �      _MainTex_ST                         _Mask_ST                    	   _Noise_ST                           _BumpMap_ST                   0     unity_ObjectToWorld                         unity_WorldToObject                  @      unity_MatrixVP                   �             _MainTex                  _Noise                  _BumpMap                _Mask                
   _ColorRamp               