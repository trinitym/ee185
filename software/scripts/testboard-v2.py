# This script is meant to run on the Adafruit M4 on the Testboard V2
# This script checks to see if high and low digital signals are
# correctly making it to the LED strip, and if so, the LEDs are then
# driven with a rainbow color. If not, the LED blinks to indicate
# an error state
# Blinking Red Fast: One LED Strip short to HIGH 
# Blinking Red Slow: One LED Strip Short to LOW
# Solid Red: Both LEDs in some error state
# Author: Aidan Chandra <aidan13@stanford.edu>

"""CircuitPython Essentials Internal RGB LED red, green, blue example"""
import time
import board
import time
import board
# import asyncio

import math
from digitalio import DigitalInOut, Direction, Pull
import neopixel
from analogio import AnalogIn


body_led = neopixel.NeoPixel(board.NEOPIXEL, 1)
body_led.brightness = 1

'''
    top = 1
    bottom = 2
'''
LEDS_PER_WING = 180
LEDS_IN_BODY = 200

bot_NEOPIXEL = None
top_NEOPIXEL = None

bot_DIO = None
top_DIO = None

bot_IN = AnalogIn(board.A1)
top_IN = AnalogIn(board.A0)

'''
These functions either enable or disable the digital pins to be regular digital
pins or neopixel pins.

We need them to be 'regular' digital pins when sensing whether or not they are 
shorted, and need them to be 'neopixel' digital pins when actually creating colors

We therefore have to claim the pin as a digital pin, and be able to un-claim it,
and same for the neopixels
'''
def initialize_LEDs():
    """
        Claims D11 and D12 as neopixel objects and sets the global variables
    """    
    global bot_NEOPIXEL, top_NEOPIXEL

    bot_NEOPIXEL = neopixel.NeoPixel(board.D11, LEDS_IN_BODY, brightness=1, auto_write=False)
    top_NEOPIXEL = neopixel.NeoPixel(board.D12, LEDS_PER_WING, brightness=1, auto_write=False)

def dereference_LEDs():
    """
        Un-claims D11 and D12 as neopixel objects and sets the global variables
    """   
    global bot_NEOPIXEL, top_NEOPIXEL

    bot_NEOPIXEL.deinit()
    top_NEOPIXEL.deinit()

def initialize_DIO():
    """
        Claims D11 and D12 as regular digital pins and sets the global variables
    """   
    global bot_DIO, top_DIO, bot_IN, top_IN

    #Initilize digital pins
    bot_DIO = DigitalInOut(board.D12)
    bot_DIO.direction = Direction.OUTPUT

    top_DIO = DigitalInOut(board.D11)
    top_DIO.direction = Direction.OUTPUT

def dereference_DIO():
    """
        Un-claims D11 and D12 as regular digital pins and sets the global variables
    """ 
    global bot_DIO, top_DIO
    bot_DIO.deinit()
    top_DIO.deinit()


'''
0: No error
1: Short to low
2: Short to high
'''
TOP_ERROR_STATE = 0
BOT_ERROR_STATE = 0

def check_Output(side: int):
    global TOP_ERROR_STATE, BOT_ERROR_STATE, top_DIO, bot_DIO, top_IN, bot_IN

    # If we want to check the top led strip
    if side == 1:

        ##Set the pin to LOW
        top_DIO.value = False
        ##Let it settle
        time.sleep(0.01)
        ##Measure the actual output going to the led strip
        val = top_IN.value

        ##If the value is NOT actually low
        if not (val < 20000):
            ## We know the pin is shorted to high
            print("TOP SHORT TO HIGH")
            TOP_ERROR_STATE = 2

            ## This step is super key - we need to turn ON the digital pin to
            # match the short, meaning minimal voltage differential and therefore
            # very little current flow, so the transistor doesn't get too hot
            top_DIO.value = True
            return False

        ## We know now that there is no short to high, so let's see if it can go low

        ##Set it to low
        top_DIO.value = True
        ##Let it settle
        time.sleep(0.01)
        ##Measure the actual output going to the LED strip
        val = top_IN.value

        ##If the value at the LED strip is NOT actually high
        if not (val > 20000):
            ## We know the pin is shorted to high
            print("TOP SHORT TO LOW")
            TOP_ERROR_STATE = 1

            ## This step is super key - we need to turn OFF the digital pin to
            # match the short, meaning minimal voltage differential and therefore
            # very little current flow, so the transistor doesn't get too hot
            top_DIO.value = False
            return False
            
        

        TOP_ERROR_STATE = 0
        return True
            
        
    ##The same exact logic
    elif side == 2:
        bot_DIO.value = False
        time.sleep(0.01)
        val = bot_IN.value
        if not (val < 20000):
            print("BOTTOM SHORT TO HIGH")
            BOT_ERROR_STATE = 2
            bot_DIO.value = True
            return False


        bot_DIO.value = True
        time.sleep(0.01)
        val = bot_IN.value
        if not (val > 20000):
            print("BOTTOM SHORT TO LOW")
            BOT_ERROR_STATE = 1
            bot_DIO.value = False
            return False

            
        

        BOT_ERROR_STATE = 0
        return True


    else:
        raise Exception("No such pin")

'''Cycles through colors (used for LED patterns)'''
def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b)

def set_led_pattern(pattern):
    for i in range(0, LEDS_PER_WING):
        top_NEOPIXEL[i] = pattern[i]
        bot_NEOPIXEL[i] = pattern[i]
    top_NEOPIXEL.show()
    bot_NEOPIXEL.show()

# Create the LED pattern of sweeping through hue
pattern = []
for i in range(0, 1024):
  pattern.append(wheel(int(i) % 256))

counter = 0

while True:

    ##Try to initialize the LED's. They may be already initialized, 
    # in which case, they will raise an exception, hence the catch
    try:
        initialize_DIO()
    except Exception as e:
        pass

    ##Update the states

    check_Output(1)
    check_Output(2)

    #Process the states

    #NO ERROR
    if BOT_ERROR_STATE == 0 and TOP_ERROR_STATE == 0:
        dereference_DIO()
        body_led[0] = (0,255,0)
        initialize_LEDs()
        while True:
            set_led_pattern(pattern[counter:])
            counter = (counter + 2) % 256
        dereference_LEDs()
    
    #EITHER ONE HAS AN ERROR
    if (TOP_ERROR_STATE == 0 and BOT_ERROR_STATE != 0) or (BOT_ERROR_STATE == 0 and TOP_ERROR_STATE != 0):
        if TOP_ERROR_STATE == 1 or BOT_ERROR_STATE == 1: 
            #SHORT TO GND
            #Blink the LED SLOW
            body_led[0] = (255,0,0)
            time.sleep(0.5)
            body_led[0] = (0,0,0)
            time.sleep(0.5)
        if TOP_ERROR_STATE == 2 or BOT_ERROR_STATE == 2: 
            #SHORT TO HIGH
            #Blink the LED FAST
            body_led[0] = (255,0,0)
            time.sleep(0.1)
            body_led[0] = (0,0,0)
            time.sleep(0.1)
    
    #BOTH BROKEN
    if TOP_ERROR_STATE != 0 and BOT_ERROR_STATE != 0:
        #KEEP THE LED RED
        body_led[0] = (255,0,0)

    



