# This script tests flapping the wings on a Fractal Flyer.
# Its timing and constants are particular to the prototype
# Phil is working on, so don't apply them generally. E.g.,
# with different motor and voltage configurations PWM needs 
# to be calibrated so the two wings move evenly.

import time
import board
import busio
import adafruit_lis3dh
import math
import digitalio
import microcontroller
import pulseio
import neopixel

MIN_SPEED = 5 #Set to lowest PWM %high that moves wing
MAX_SPEED = 90 #Set to PWM %high that represents max speed we want the wings to move
left_wing_high = 45 #flap left wing this many degrees above level
left_wing_low = -45  #flap left wing this many degrees below level
left_wing_target_angle = left_wing_high
period = 20 #Period (s) before reversing target angle
LEDS_PER_WING = 180
LEDS_IN_BODY = 100
MOTOR_PWM_FREQUENCY = 20000

#SPI setup: used for body (center) accelerometer
from digitalio import DigitalInOut, Direction
spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
cs_center = DigitalInOut(board.D6)
sensor_center = adafruit_lis3dh.LIS3DH_SPI(spi, cs_center)

#I2C setup: used for wing accelerometers
i2c = busio.I2C(board.SCL, board.SDA)
sensor_right = adafruit_lis3dh.LIS3DH_I2C(i2c)
sensor_left = adafruit_lis3dh.LIS3DH_I2C(i2c, address=0x19)

#LED setup
left_wing_lights = neopixel.NeoPixel(board.A0, LEDS_PER_WING, brightness=1, auto_write=False)
body_lights = neopixel.NeoPixel(board.A4, LEDS_IN_BODY, brightness=1, auto_write=False)

motor_left_backward = pulseio.PWMOut(board.A1, frequency=MOTOR_PWM_FREQUENCY)
motor_left_forward = pulseio.PWMOut(board.A3, frequency=MOTOR_PWM_FREQUENCY)

motor_right_forward = pulseio.PWMOut(board.RX, frequency=MOTOR_PWM_FREQUENCY)
motor_right_backward = pulseio.PWMOut(board.TX, frequency=MOTOR_PWM_FREQUENCY)
'''Returns the acceeleration of the entire bird (top plate acceleration)'''
def get_bird_accel():
        return sensor_center.acceleration

'''Gets the angle of the left wing if left=True, right if left=False, after subtracting out the
acceleration of the entire bird.'''
def get_wing_angle(left):
    global sensor_left, sensor_right
    bird_x, bird_y, bird_z = get_bird_accel()
    
    if (bird_x < -2.0 or bird_x > 2.0 or bird_y < -2.0 or bird_y > 2.0 or bird_z < 9.0 or bird_z > 11.0):
        raise Exception("Bird acceleration is wrong: x: ", bird_x, "y:", bird_y, "z:", bird_z) from None
    
    accel_x = 0.0
    accel_y = 0.0
    accel_z = 0.0
    if left:
        accel_x, accel_y, accel_z = sensor_left.acceleration
    else:
        accel_x, accel_y, accel_z = sensor_right.acceleration

    if accel_x == 0.0 and accel_y == 0.0 and accel_z == 0.0:
        print("left: ", left, "x:", accel_x, 'y:', accel_y, 'z:', accel_z)
        print("Resetting accelerometer!")
        if left:
            sensor_left = adafruit_lis3dh.LIS3DH_I2C(i2c, address=0x19)
        else:
            sensor_right = adafruit_lis3dh.LIS3DH_I2C(i2c)

    if accel_z < 0.0:
        if left:
            print("left x:", accel_x, "y:", accel_y, "z:", accel_z)
            raise Exception('Left wing is past 90 degrees') from None
        else:
            print("right x:", accel_x, "y:", accel_y, "z:", accel_z)
            raise Exception('Right wing is past 90 degrees') from None

    if accel_z == 0:
        accel_z = 0.0001
    accel_x -= bird_x
    accel_y -= bird_y
    #print("dx:", accel_x, 'dy:', accel_y, 'dz:', accel_z)

    tan_pitch = accel_y / accel_z
    #print('tan:', tan_pitch)

    angle_rads = math.atan(tan_pitch)
    angle_degrees = angle_rads * 180 / math.pi
    #print('degrees:', angle_degrees)
    return angle_degrees * -1

'''converts an integer percent (0-100) to the u16 that corresponds to a duty cycle that % high'''
def percent_to_duty_cycle(percent):
    return percent * 65535//100

'''Set the direction and speed for the passed motor (left or right)'''
def set_motor_direction_and_speed(left, up, percent):
    if left:
        if up:
            motor_left_forward.duty_cycle = percent_to_duty_cycle(int(percent * 9 / 10))
            motor_left_backward.duty_cycle = 0
        else:
            motor_left_backward.duty_cycle = percent_to_duty_cycle(int(percent * 8 / 10))
            motor_left_forward.duty_cycle = 0
    else:
        if up:
            motor_right_forward.duty_cycle = percent_to_duty_cycle(percent)
            motor_right_backward.duty_cycle = 0
        else:
            motor_right_backward.duty_cycle = percent_to_duty_cycle(percent)
            motor_right_forward.duty_cycle = 0

'''Set the angle of the passed wing to the desired angle in degrees'''
def set_wing_angle(left, degrees):
    cur_angle = 0.0
    try:
        cur_angle = get_wing_angle(left)
    except Exception as ex:
        print("Wing past 90 degrees. Aborting action.")
        print(ex)
        set_motor_direction_and_speed(left, True, 0)
        return
    if cur_angle > 50.0 or cur_angle < -50.0:
        print("Wing is at ", cur_angle, " so hold to be safe")
        set_motor_direction_and_speed(left, True, 0)
        return

    diff = abs(cur_angle - degrees)
    speed = 0
    ERR_THRESHOLD_DEGREES = 2 #Margin of error (degrees) within which wing will not be moved
    BEGIN_SLOW_DEGREES = 5 #Rotational distance (degrees) from desired angle at which
                            #the motor should move at less than full speed
    if diff < ERR_THRESHOLD_DEGREES:
        speed = 0
    elif diff < BEGIN_SLOW_DEGREES:
        speed = int(((diff - ERR_THRESHOLD_DEGREES)/(BEGIN_SLOW_DEGREES-1))*(MAX_SPEED-MIN_SPEED) + MIN_SPEED)
    else:
        speed = MAX_SPEED

    if cur_angle < degrees:
        set_motor_direction_and_speed(left, True, speed)
    else:
        set_motor_direction_and_speed(left, False, speed)

start = (int)(time.monotonic())
last_passed = -1

# Main loop
while True:
    passed = (int)(time.monotonic()) - start

    #TODO: Pattern LEDS here. For now, simulate with delay
    time.sleep(0.01)
    #If desired, smoother operation could be achieved by only changing lights with wings stationary

    #TODO: Any communication here (change behavior by modifying global variables)

    if passed%period == 0 and passed != last_passed:
        left_wing_target_angle = -left_wing_target_angle
        print("Wings moving to angle", left_wing_target_angle)
        last_passed = passed

    set_wing_angle(True, left_wing_target_angle)
    set_wing_angle(False, left_wing_target_angle)

