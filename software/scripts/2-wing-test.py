# This script is used to test the wing motion and lighting.
# It moves both wings from -45 to 45 degrees with a slight pause
# at each point. It also lights up both wings with a rotating
# segment of the hue wheel of HSV color.
#
# It assumes that the wing sensors are connected with I2C and
# the body sensor is connected with SPI. The left wing LEDs data
# line is # on A2 and the right wing LEDs is on A0. The CS line
# for the body sensor is D6.
#
# This is derived from Hudson's script for testing the wings. It's
# been adjusted to stop if the wings go past 90 degrees (to make
# sure cords don't break).
#
# Author: Philip Levis <pal@cs.stanford.edu>
# Author: Hudson Ayers <hayers@stanford.edu>

import time
import board
import busio
import adafruit_lis3dh
import math
import digitalio
import microcontroller
import pulseio
import neopixel

MIN_SPEED = 15 #Set to lowest PWM %high that moves wing
MAX_SPEED = 90 #Set to PWM %high that represents max speed we want the wings to move
LEDS_PER_WING = 80
LEDS_IN_BODY = 100
MOTOR_PWM_FREQUENCY = 20000

#SPI setup:
from digitalio import DigitalInOut, Direction
spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
i2c = busio.I2C(board.SCL, board.SDA)

#right wing accel init
sensor_right = adafruit_lis3dh.LIS3DH_I2C(i2c)

#left wing accel init
sensor_left = adafruit_lis3dh.LIS3DH_I2C(i2c, address=0x19)

#center plate accel init
cs_center = DigitalInOut(board.D6)
sensor_center = adafruit_lis3dh.LIS3DH_SPI(spi, cs_center)


#LED setup
left_wing_lights = neopixel.NeoPixel(board.A2, LEDS_PER_WING, brightness=1, auto_write=False)
right_wing_lights = neopixel.NeoPixel(board.A0, LEDS_PER_WING, brightness=1, auto_write=False)
body_lights = neopixel.NeoPixel(board.A4, LEDS_IN_BODY, brightness=1, auto_write=False)

motor_left_backward = pulseio.PWMOut(board.A3, frequency=MOTOR_PWM_FREQUENCY)
motor_left_forward = pulseio.PWMOut(board.A1, frequency=MOTOR_PWM_FREQUENCY)
motor_right_backward = pulseio.PWMOut(board.TX, frequency=MOTOR_PWM_FREQUENCY)
motor_right_forward = pulseio.PWMOut(board.RX, frequency=MOTOR_PWM_FREQUENCY)

'''Returns the acceeleration of the entire bird (top plate acceleration)'''
def get_bird_accel():
        return sensor_center.acceleration

'''Gets the angle of the left wing if left=True, right if left=False, after subtracting out the
acceleration of the entire bird.'''
def get_wing_angle(left):
    bird_x, bird_y, bird_z = get_bird_accel()
    accel_x = 0.0
    accel_y = 0.0
    accel_z = 0.0
    if left:
        accel_x, accel_y, accel_z = sensor_left.acceleration
    else:
        accel_x, accel_y, accel_z = sensor_right.acceleration

    if accel_z == 0:
        accel_z = 0.0001
    accel_x -= bird_x
    accel_y -= bird_y
   
    if (accel_z < 0):
        raise Exception("Wing is past 90 degrees")

    tan_pitch = accel_y / accel_z
    angle_rads = math.atan(tan_pitch)
    angle_degrees = angle_rads * 180 / math.pi
    return angle_degrees * -1

'''Cycles through colors (used for LED patterns)'''
def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b)

'''converts an integer percent (0-100) to the u16 that corresponds to a duty cycle that % high'''
def percent_to_duty_cycle(percent):
    return percent * 65535//100

'''Set the direction and speed for the passed motor (left or right)'''
def set_motor_direction_and_speed(left, up, percent):
    if left:
        if up:
            motor_left_forward.duty_cycle = percent_to_duty_cycle(percent)
            motor_left_backward.duty_cycle = 0
        else:
            motor_left_backward.duty_cycle = percent_to_duty_cycle(percent)
            motor_left_forward.duty_cycle = 0
    else:
        if up:
            motor_right_forward.duty_cycle = percent_to_duty_cycle(percent)
            motor_right_backward.duty_cycle = 0
        else:
            motor_right_backward.duty_cycle = percent_to_duty_cycle(percent)
            motor_right_forward.duty_cycle = 0

'''Set the angle of the passed wing to the desired angle in degrees'''
def set_wing_angle(left, degrees):
    cur_angle = get_wing_angle(left)
    diff = abs(cur_angle - degrees)
    speed = 0
    ERR_THRESHOLD_DEGREES = 2 #Margin of error (degrees) within which wing will not be moved
    BEGIN_SLOW_DEGREES = 10 #Rotational distance (degrees) from desired angle at which
                            #the motor should move at less than full speed
    if diff < ERR_THRESHOLD_DEGREES:
        speed = 0
    elif diff < BEGIN_SLOW_DEGREES:
        speed = int(((diff - ERR_THRESHOLD_DEGREES)/(BEGIN_SLOW_DEGREES-1))*(MAX_SPEED-MIN_SPEED) + MIN_SPEED)
    else:
        speed = MAX_SPEED

    if cur_angle < degrees:
        set_motor_direction_and_speed(left, False, speed)
    else:
        set_motor_direction_and_speed(left, True, speed)

def set_wing_led_pattern(left, pattern):
    if left:
        for i in range(0, LEDS_PER_WING):
            left_wing_lights[i] = pattern[i]
        left_wing_lights.show()
    else:
        for i in range(0, LEDS_PER_WING):
            right_wing_lights[i] = pattern[i]
        right_wing_lights.show()


pattern = []
for i in range(0, 512):
  pattern.append(wheel(i % 256))



start = (int)(time.monotonic())
wing_target_angle = 50 #flap left wing this many degrees above/below level
period = 8 #Period (s) before reversing target angle
last_passed = -1
counter = 0
# Main loop
while True:
    passed = (int)(time.monotonic()) - start

    #TODO: Pattern LEDS here. For now, simulate with delay
    #If desired, smoother operation could be achieved by only changing lights with wings stationary

    set_wing_led_pattern(False, pattern[counter:])
    set_wing_led_pattern(True, pattern[counter:])
    counter = (counter + 1) % 256
    #TODO: Any communication here (change behavior by modifying global variables)

    if passed%period == 0 and passed != last_passed:
        wing_target_angle = wing_target_angle * -1
        last_passed = passed
    
    try:
        set_wing_angle(True, wing_target_angle)
        set_wing_angle(False, wing_target_angle)
        bird_accel = get_bird_accel()
        bird_accel_x, bird_accel_y, bird_accel_z = bird_accel
        angle_left = get_wing_angle(True)
        angle_right = get_wing_angle(False)
        print('wing target: {0:0.2f}, left wing angle: {1:0.2f}, right wing angle: {2:0.2f}'.format(wing_target_angle, angle_left, angle_right))
    except Exception as ex:
        print("Exception when setting angle:", ex)
        set_motor_direction_and_speed(False, False, 0)
        set_motor_direction_and_speed(True, False, 0)



