# Course Materials for Spring 2022

This directory stores the course materials for the Spring 2022
offering of EE285/CS241. Directory structure:
  - assignments: Assignment handouts.
  - body-shell: Workspace for the body shell group
  - mounting: Workspace for the mounting group
  - wings: Worksspace for the wings group


